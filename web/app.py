# Copyright (C) 2023 Internet Systems Consortium, Inc. ("ISC")
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# SPDX-FileCopyrightText: 2023 Internet Systems Consortium
# SPDX-License-Identifier: MPL-2.0

from flask import Flask, abort, render_template, request, send_from_directory, flash, redirect, url_for
from flask_wtf import FlaskForm, CSRFProtect
from werkzeug.exceptions import RequestEntityTooLarge
from wtforms import FileField, SubmitField, RadioField, TextAreaField, HiddenField
from wtforms.widgets import TextArea
from werkzeug.utils import secure_filename
from subprocess import Popen, PIPE, TimeoutExpired
from shutil import copyfile
from typing import Tuple

import uuid
import os
import markdown

app = Flask(__name__)
csrf = CSRFProtect(app)

# This specifies the maximum size of the upload file. Anything larger will be rejected.
app.config['MAX_CONTENT_LENGTH'] = 10 * 1024 * 1024

# This specifies the upload location for configs to be uploaded.
app.config['UPLOAD_FOLDER'] = "data/"

# This specifies the location of files the user chose to share with ISC. This must be relative path.
app.config['SAVED_FOLDER'] = "saved/"

# This specifies the location of Keama binary.
app.config['KEAMA_PATH'] = 'keama/keama'

# specifies timeout of keama execution in seconds
app.config['TIMEOUT'] = 30

# Force obfuscating paths in Keama stdout and stderr even in debug mode.
app.config['FORCE_OBFUSCATE_PATHS'] = False

# --- Code starts here ------------------


class UploadFileForm(FlaskForm):
    """Represents a web form and its fields. This is used to upload ISC-DHCP config."""
    file = FileField("File")
    configtxt = TextAreaField('Paste contents of your ISC DHCP config here', widget=TextArea())
    family = RadioField('Configuration type:', choices=[('v4', 'DHCPv4'), ('v6', 'DHCPv6')], default='v4')
    submit = SubmitField("Migrate!")


class ShareForm(FlaskForm):
    comment = TextAreaField("Share any thoughts, comments or contact info if you wish to be contacted")
    submit = SubmitField("Share your config with ISC")
    input_file = HiddenField()  # ISC DHCP config file shared by user


def run_cmd(binfile, params, env=None):
    """Runs file specified as binfile, passes all parameters.
    Returns a tuple of returncode, stdout, stderr"""
    par = [binfile]
    if params and len(params):
        par += params

    # @todo: set passed env variable, if any
    try:
        p = Popen(par, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate(timeout=app.config['TIMEOUT'])
        exit_code = p.returncode
        stdout = stdout.decode("utf-8")
        stderr = stderr.decode("utf-8")
    except PermissionError as e:
        exit_code, stdout, stderr = -1, "", str(e)
    except TimeoutExpired as e:
        # We should kill the process and try to retrieve any data before the process
        # was killed. These logs may possibly indicate what exactly triggered keama
        # to go into infinite loop. This clean-up follows the python recommended behavior
        # See https://docs.python.org/3/library/subprocess.html#subprocess.Popen.communicate
        # for details regarding timeout handling.
        p.kill()
        stdout, stderr = p.communicate()
        exit_code = -1
        stdout = stdout.decode("utf-8")
        stderr = stderr.decode("utf-8")
        stderr = f"Keama was not able to complete migration in {app.config['TIMEOUT']}s.\n"\
            f"Error details: {str(e)}\n{stderr}"
    return exit_code, stdout, stderr


def migrate_config(input_file: str, v6: bool = False) -> Tuple[str, int, str, str]:
    """Attempts to migrate isc-dhcp config by running Keama on specified input_file.
    returns:
    - filename of the converted file, status_code (0..255), content of keama
    output file path, stdout, content of keama stderr
    """

    # try absolute path to keama binary
    binary = os.path.abspath(app.config['KEAMA_PATH'])

    if not os.path.isfile(binary) or not os.access(binary, os.X_OK):
        return "", -1, "", "Unable to find KEAMA binary, make sure your KEAMA_PATH is set properly."

    # Let's remove the extension and replace it with json
    output_file = os.path.splitext(input_file)[0] + ".json"

    params = ["-6" if v6 else "-4", "-i", input_file, "-o", output_file]

    print(f"\nRunning command: {binary} {' '.join(params)}")

    result = run_cmd(binary, params)

    if not os.access(output_file, os.R_OK):
        print("ERROR: Conversion failed (no output file found).")
        output_file = ""

    # Let's return status code, stdout, stderr
    return output_file, result[0], result[1], result[2]


def read_file(filename: str) -> str:
    """Reads file and return its content."""
    with open(filename, "r") as f:
        content = f.read()
    return content


def write_file(filename: str, content: str):
    """Writes content to a file"""
    with open(filename, "w") as f:
        f.write(content)


def obfuscate_paths(path: str, content: str):
    """Obfuscates given path in the given content string"""
    if content:
        return content.replace(path, "(...)/")
    else:
        return None


def gen_secret_key_only_once():
    """
    Generates random secret key and saves it to file to make it persistent.
    If the file already exists, and it has some content, it will NOT be overwritten.
    If you want to regenerate your secret code, please delete the secret_key.py file
    manually.
    """
    filename = "secret_key.py"
    abs_path = os.path.join(os.path.dirname(__file__), filename)
    if os.path.exists(abs_path) and os.path.getsize(abs_path) > 10:
        # file with secret_key was already generated
        return
    f = open(abs_path, "w")
    f.write(f"SECRET_KEY = '{str(uuid.uuid1())}'")
    f.close()


def gen_changelog():
    """
    Generates templates/changelog.html file from ChangeLog.md
    """
    html_abs_path = os.path.join(os.path.dirname(__file__), "templates", "changelog.html")
    md_abs_path = os.path.join(os.path.dirname(__file__), os.path.pardir, "ChangeLog.md")
    if not os.path.exists(md_abs_path):
        # this is a workaround for when deploying the keama web interface from keama-docker dockerfile
        md_abs_path = os.path.join(os.path.dirname(__file__), "ChangeLog.md")
    if os.path.exists(md_abs_path):
        markdown.markdownFromFile(output=html_abs_path, input=md_abs_path)


# If needed, generate a random secret key and load it from file
gen_secret_key_only_once()
app.config.from_pyfile("secret_key.py")

# Load and overwrite config from env variables. This could be very useful in production e.g.
# if you set SECRET_KEY as env var FLASK_SECRET_KEY, it will be read and loaded here.
app.config.from_prefixed_env()
gen_changelog()

# --- route handlers below. Each of those functions handles one specific URL


@app.route('/')
def async_home():
    """Handles the initial home page with async form."""
    return render_template("async_home.html", form=FlaskForm())


@app.route('/offline')
def offline():
    """Handles the page with info about keama offline usage."""
    return render_template("async_offline.html")


@app.route('/about')
def about():
    """Handles the page with more info about keama."""
    return render_template("async_about.html")


@app.route('/migrate', methods=["POST"])
def migrate():
    """Handles async HTTP POST request with form data and config file needed for migration."""
    file = None
    content = None
    share_config = False
    family = "v4"
    comment = None

    try:
        if request.files:
            if request.files.getlist("file"):
                file = request.files.getlist("file")[0]
        if request.form:
            if request.form.getlist("configtxt"):
                content = request.form.getlist("configtxt")[0].strip()
            if request.form.getlist("family"):
                family = request.form.getlist("family")[0]
            if request.form.getlist("share") and request.form.getlist("share")[0] == "yes":
                share_config = True
                if request.form.getlist("comment"):
                    comment = request.form.getlist("comment")[0].strip()
    except RequestEntityTooLarge:
        return "Sorry, the file you tried to upload is too large. If you really tried to upload an ISC DHCP" \
               " configuration file and it is that large, you need to download and run KeaMA locally.", 413

    if not file and not content:
        return "You did not provide ISC DHCP config for migration! Please choose a valid config file or copy-paste " \
               "your config to Config contents.", 400

    if file.filename != "" and content != "":
        flash("You provided config file and pasted some config content into the textarea. Please be informed that "
              "file upload takes precedence over the textarea. Only uploaded file content was converted.", "info")

    # Generate the absolute path.
    filename_root = secure_filename(str(uuid.uuid1()))
    input_config = filename_root + '.conf'
    upload_folder_path = app.config['UPLOAD_FOLDER']
    if upload_folder_path[0] != '/':
        upload_folder_path = os.path.join(os.path.dirname(__file__), upload_folder_path)
    input_config_abs_path = os.path.join(upload_folder_path, input_config)

    if file.filename != "":
        # User provided a file, let's use that.
        file.save(input_config_abs_path)
        file.close()
        print(f"Wrote {os.path.getsize(input_config_abs_path)} bytes to {input_config_abs_path} provided in the file "
              f"upload field.")
        # User provided empty file, so let's render appropriate feedback.
        if os.path.getsize(input_config_abs_path) == 0:
            return "You provided empty ISC DHCP config file!", 400
    else:
        # The alternative is the user provided content in the TextArea field. Let's try to use that.
        write_file(input_config_abs_path, content)
        print(f"Wrote {os.path.getsize(input_config_abs_path)} bytes to {input_config_abs_path} provided in the "
              f"textarea field.")

    output_config, exit_code, stdout, stderr = migrate_config(input_config_abs_path, family == "v6")

    print(f"Conversion result: input_file={input_config}, output_file={output_config}, exit code={exit_code}, "
          f"stdout len={len(stdout)}, stderr={len(stderr)}")

    if len(output_config) and os.path.exists(output_config) and os.path.getsize(output_config) > 0:
        download_file = os.path.split(output_config)[-1]
        out_content = read_file(output_config)
    else:
        download_file = None
        out_content = None

    if not len(stdout):
        stdout = None

    if not len(stderr):
        stderr = None

    # obfuscate upload_folder_path only in production OR when FORCE_OBFUSCATE_PATHS set to True
    if app.config['FORCE_OBFUSCATE_PATHS'] or (not app.debug
                                               and os.environ.get("FLASK_RUN_FROM_CLI") != "true"
                                               and not app.testing):
        stdout = obfuscate_paths(upload_folder_path, stdout)
        stderr = obfuscate_paths(upload_folder_path, stderr)

    if out_content:
        flash("Your config was successfully migrated! Thank you for using ISC software.", "success")
        # store shared config only if it was a valid config
        if share_config:
            print(f"user shared comment='{comment}' and file='{input_config}'")
            if len(comment):
                comment_file = filename_root + ".txt"
                comment_file_abs_path = os.path.join(os.path.dirname(__file__),
                                                     app.config['SAVED_FOLDER'],
                                                     comment_file)
                write_file(comment_file_abs_path, comment)
                print(f"Wrote {len(comment)}-bytes long comment to {comment_file_abs_path}\n")
            else:
                print(f"Comment was not provided, skipping")

            dst = os.path.join(os.path.dirname(__file__), app.config['SAVED_FOLDER'], input_config)

            try:
                copyfile(input_config_abs_path, dst)
                print(f"Copied file {input_config_abs_path} to {dst}")
                flash(f"Your config file has been shared with ISC. Thank you for your help in making the migration "
                      f"software better!", "info")
            except FileNotFoundError:
                print(f"ERROR: Tried to copy non-existent file {input_config_abs_path}")
                flash("There was a problem with sharing your ISC DHCP config file.", "warning")
    else:
        flash("There was a problem with the migration. Please check stderr below. You can modify your input config and"
              " <a href='" + url_for("async_home") + "'>try the migration again</a>.", "warning")

    return render_template("async_migrated.html", family=family, file=download_file, exit_code=exit_code,
                           stdout=stdout, stderr=stderr, out_content=out_content, share=share_config)


@app.route("/legacy")
def home():
    """Handles the initial home page."""
    form = UploadFileForm()
    return render_template('welcome.html', form=form)


@app.route('/keama', methods=["POST"])
def keama():
    """Handles the /keama URL, which expects a config file and does the actual migration."""

    try:
        if 'file' not in request.files:
            flash("You did not provide ISC DHCP config for migration!", "error")
            return redirect(url_for('home'))

        file = request.files['file']
    except RequestEntityTooLarge:
        flash("Sorry, the file you tried to upload is too large. If you really tried to upload an ISC DHCP"
              " configuration file and it is that large, you need to download and run KeaMA locally.", "warning")
        return redirect(url_for('home'))

    content = request.form['configtxt'].strip()
    # If the user does not select a file and the textarea field is empty, the browser submits an
    # empty file without a filename. This is a user error. There's nothing to convert.
    if file.filename == '' and content == '':
        flash("You did not provide ISC DHCP config for migration!", "error")
        return redirect(url_for('home'))

    if file.filename != "" and content != "":
        flash("You provided config file and pasted some config content into the textarea. Please be informed that "
              "file upload takes precedence over the textarea. Only uploaded file content was converted.", "info")

    # Generate the absolute path.
    input_file = secure_filename(str(uuid.uuid1())) + '.conf'
    upload_folder_path = app.config['UPLOAD_FOLDER']
    if upload_folder_path[0] != '/':
        upload_folder_path = os.path.join(os.path.dirname(__file__), upload_folder_path)
    input_file_path = os.path.join(upload_folder_path, input_file)

    if file.filename != "":
        # User provided a file, let's use that.
        file.save(input_file_path)
        file.close()
        print(f"Wrote {len(content)} bytes to {input_file_path} provided in the file upload field.")
        # User provided empty file, so let's render appropriate feedback.
        if os.path.getsize(input_file_path) == 0:
            flash("You provided empty ISC DHCP config file!", "error")
            return redirect(url_for('home'))
    else:
        # The alternative is the user provided content in the TextArea field. Let's try to use that.
        write_file(input_file_path, content)
        print(f"Wrote {len(content)} bytes to {input_file_path} provided in the textarea field.")

    output_file, exit_code, stdout, stderr = migrate_config(input_file_path, request.form['family'] == 'v6')

    print(f"Conversion result: input_file={input_file}, output_file={output_file}, exit code={exit_code}, "
          f"stdout len={len(stdout)}, stderr={len(stderr)}")

    if len(output_file) and os.path.exists(output_file) and os.path.getsize(output_file) > 0:
        file = os.path.split(output_file)[-1]
        out_content = read_file(output_file)
    else:
        file = None
        out_content = None

    if not len(stdout):
        stdout = None

    if not len(stderr):
        stderr = None

    # obfuscate upload_folder_path only in production OR when FORCE_OBFUSCATE_PATHS set to True
    if app.config['FORCE_OBFUSCATE_PATHS'] or (not app.debug
                                               and os.environ.get("FLASK_RUN_FROM_CLI") != "true"
                                               and not app.testing):
        stdout = obfuscate_paths(upload_folder_path, stdout)
        stderr = obfuscate_paths(upload_folder_path, stderr)

    form = ShareForm(input_file=input_file)

    if out_content:
        flash("Your config was successfully migrated!", "success")

    return render_template("migrated.html", family=request.form['family'], file=file, exit_code=exit_code,
                           stdout=stdout, stderr=stderr, out_content=out_content, form=form)


@app.route('/download/<string:path>')
def send_file(path):
    """Handles download of the converted file."""
    path = secure_filename(path)
    return send_from_directory(app.config['UPLOAD_FOLDER'], path)


@app.route('/share', methods=["POST"])
def share_file():
    """Handles sharing of the file with ISC."""
    if not request.form or not request.form.getlist("input_file"):
        print(f"ERROR: No input_file shared via form!")
        abort(404)
    comment = ""
    if request.form and request.form.getlist("comment"):
        comment = request.form['comment']
    path = request.form['input_file']
    print(f"user shared comment='{comment}' and file='{path}'")

    if not len(path):
        print(f"ERROR: input_file shared via form has length 0!")
        abort(404)

    if len(comment):
        comment_file = os.path.splitext(secure_filename(path))[0] + ".txt"
        comment_file = os.path.join(os.path.dirname(__file__), app.config['SAVED_FOLDER'], comment_file)
        write_file(comment_file, comment)
        print(f"Wrote {len(comment)}-bytes long comment to {comment_file}\n")
    else:
        print(f"Comment was not provided, skipping")

    src = os.path.join(os.path.dirname(__file__), app.config['UPLOAD_FOLDER'], secure_filename(path))
    dst = os.path.join(os.path.dirname(__file__), app.config['SAVED_FOLDER'], secure_filename(path))
    try:
        print(f"Copied file {src} to {dst}")
        copyfile(src, dst)
        code = 200
    except FileNotFoundError:
        print(f"ERROR: Tried to copy non-existent file {path}")
        path = "(invalid)"
        code = 404

    return render_template("share.html", file=path), code


@app.route('/flask-health-check')
def flask_health_check():
    """
    Handles health check, used mainly for monitoring Docker container when run in a container.
    Currently does nothing.
    """
    return "success"


@app.errorhandler(405)
def handle_method_not_allowed_error(error):
    return render_template("error405.html"), 405


@app.errorhandler(404)
def handle_not_found_error(error):
    return render_template("error404.html"), 404


if __name__ == '__main__':
    app.run(debug=False, port=8080)
