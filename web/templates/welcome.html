{% extends 'base.html' %}

{% block content %}
<p>
ISC has developed the Kea Migration Assistant (KeaMA) tool to help users migrate from the legacy ISC DHCP server
to the <a href="https://www.isc.org/kea/">Kea DHCP server.</a> This tool analyzes a valid ISC DHCP server configuration
file and provides an equivalent configuration file for a Kea DHCP server. The resulting file is a *starting point* for
your Kea configuration, but it will probably require editing before use.
</p>

<div class="col-container">
    <div class="col">
        <h3>Online Migration</h3>
        <p>This web interface is the easiest way to translate your old configuration files. </p>

        <h3>Using the Tool</h3>
        <p>
        Upload your ISC DHCP server configuration, which is typically stored in <code>/etc/dhcp/dhcpd.conf</code>.
        If you use both IPv4 and IPv6 in your network, you will have two separate files; this web tool analyzes a single
        file at a time. Upload your file using the form below, and make sure you check the appropriate IP family (DHCPv4
        or DHCPv6). There is a limit on the file size for the upload. If your dhcpd.conf file is too big, you may have
        to run the KeaMA tool locally instead of using this web page. Once your dhcpd.conf file is uploaded, use the
        <code>MIGRATE!</code> button to trigger the translation. Your results will be displayed and you can download the
        output file. If the file uploaded is not a valid ISC DHCP configuration file, the translation will fail and
        error messages will be displayed. Those can be used to correct the input configuration file so you can try again.
        </p>

        <h3>Data Privacy Concerns</h3>
        <p>
        Uploaded files are permanently removed from the server after 10 minutes. If you want to share your configuration
        with ISC to improve the KeaMA software, there is an option to do so; none of your information is preserved unless
        you specifically allow it.
        </p>

        <h3>Migrate!</h3>

        <form method="post" action="{{ url_for('keama') }}" enctype="multipart/form-data">
            {{form.hidden_tag()}}

            What kind of configuration would you like to migrate: {{form.family()}}

            You can either upload your ISC DHCP configuration file here: {{form.file()}}<br/><br/>

            ... or paste its contents here. Uploading a file is generally safer, as it avoids problems
            with line wraps, copying too much/not enough data, etc.<br/>
            {{form.configtxt(cols="80", rows="25")}}<br/>

            Once you are ready, hit this button: {{form.submit()}}
        </form>
    </div>
    <div class="col">
        <h3>Off-Line Migration</h3>
        <p>This page offers a hosted instance of the Kea Migration Assistant (KeaMA) tool for your convenience. You can achieve the same result by
        downloading the software and running it yourself. It is more
        complex to download and install this software to your system; however,
        your data never leaves your system. This might be preferable if you are concerned about data privacy.</p>
        <p>The Kea Migration Assistant can be built from source, installed as a pre-compiled package from ISC’s package
        repository, or run as a Docker container.</p>
        <ul>
            <li><a href="https://cloudsmith.io/~isc/repos/keama/packages/"> Kea Migration Assistant packages</a></li>
            <li><a href="https://gitlab.isc.org/isc-projects/keama"> Kea Migration Assistant sources</a></li>
            <li><a href="https://gitlab.isc.org/isc-projects/keama-docker">Kea Migration Assistant Dockerfile</a></li>
        </ul>
    </div>
</div>

<h3>Limitations</h3>
<p>This utility translates most ISC DHCP configuration elements to the appropriate format for Kea. However, some
    elements of an ISC DHCP configuration file cannot be automatically translated to Kea format, either because some
    features of ISC DHCP are unsupported in Kea, or because Kea implements that functionality differently. In areas where the
    utility is unable to translate the configuration, it inserts diagnostic messages to highlight what was not
    translated, with references to issues in the Kea GitLab that provide more detail. These sections of the
    configuration will require manual review and adjustment.</p>

<p>This tool does not translate the DHCP active lease file. There is an experimental project to translate lease files,
    the <a href="https://gitlab.isc.org/isc-projects/keama-leases">KeaMA Leases tool.</a>
</p>

<h3>Next Steps</h3>
<p>Open the output file and look for log messages, which may include links to ISC GitLab issues with more
    information.</p>

<p>The major configuration areas that will likely require redesign are the failover or high-availability solution,
    client classification, and host reservations. Kea has an alternative to the DHCPv4 failover draft implemented in ISC
    DHCP: the Kea feature is called “High Availability,” and it works equally well for both DHCPv4 and DHCPv6. Kea does
    support client classification, but there is no equivalent for ISC DHCP’s hyper-flexible permit/deny scripting
    language. The option inheritance hierarchy in Kea is different than in ISC DHCP, and the configuration for custom
    vendor-specific options is also different. Kea has robust support for host reservations, but in ISC DHCP all host
    reservations are global. In Kea, host reservations are by default per-subnet, although global host reservations are
    also supported. The following resources may help with understanding your options for enabling these features in
    Kea:</p>

<ul>
    <li><a href="https://kea.readthedocs.io/en/latest/"> Kea Administrator Reference Manual</a></li>
    <li><a href="https://kb.isc.org/docs/migrating-from-isc-dhcp-to-kea-dhcp-using-the-migration-assistant"> Migrating from ISC DHCP to Kea</a></li>
    <li><a href="https://kb.isc.org/docs/aa-01617"> Kea High Availability vs ISC DHCP Failover</a></li>
    <li><a href="https://kb.isc.org/docs/kea-configuration-sections-explained"> Kea Configuration Introduction</a></li>
    <li><a href="https://kb.isc.org/docs/understanding-client-classification"> Understanding Client Classification in Kea</a></li>
</ul>

<p>Once you think your configuration file is ready, load it into the respective Kea DHCPv4 or DHCPv6 server and run the
configuration checkers. </p>

<p>When you are ready to cut over from your old ISC DHCP server to Kea, you may want to also translate your active
leases. For that, we recommend using the <a href="https://gitlab.isc.org/isc-projects/keama-leases">KeaMA Leases tool.</a></p>

<h3>Getting Help</h3>
<p>If you have problems using this tool, or want to report crashes or bugs with the tool, please open an issue in the
<a href="https://gitlab.isc.org/isc-projects/keama/-/issues/new">Kea GitLab project</a></p>

<p>For advice on how to rewrite configuration elements that cannot be translated by machine, or other topics not addressed here, please post
your questions on the <a href="https://lists.isc.org/mailman/listinfo/kea-users">kea-users mailing list.</a></p>

<p>Users whose organizations are interested in purchasing professional support services from ISC are encouraged to contact
us at <a href="https://www.isc.org/contact">https://www.isc.org/contact.</a></p>

{% endblock %}\
