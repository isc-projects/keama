#include "data.h"

/// @brief prints brief info about the subnet being converted
/// @param subnet pointer to parsed element subnet
/// @param begin_line first line of the subnet definition
/// @param end_line last line of the subnet definition
void print_subnet(struct element* subnet, int begin_line, int end_line);

/// @brief prints brief info about the host being converted
/// @param subnet pointer to parsed element host
/// @param begin_line first line of the host definition
/// @param end_line last line of the host definition
void print_host(struct element* subnet, int begin_line, int end_line);

/// @brief prints brief info about the shared network being converted
/// @param subnet pointer to parsed element shared network
/// @param begin_line first line of the shared network definition
/// @param end_line last line of the shared network definition
void print_network(struct element* network, int begin_line, int end_line);
