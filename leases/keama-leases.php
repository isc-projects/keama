<!-- Copyright (C) 2018-2023 Internet Systems Consortium, Inc. ("ISC")

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<?php
 $src = "";
 if(array_key_exists('ta',$_POST))
   $src = $_POST['ta'];
 if(!empty($src)) { 
   $fname = "c:\a.leases";
   file_put_contents($fname, $_POST['ta']);
   $exec = "c:\Windows\py.exe C:\Apache24\htdocs\dhcp\dhcp2kea.py -v " . $fname . " 2>&1";
   passthru($exec);
   exit;   
 } else {
   $src = <<<'SRC'
# The format of this file is documented in the dhcpd.leases(5) manual page.
# This lease file was written by isc-dhcp-4.4.2

# authoring-byte-order entry is generated, DO NOT DELETE
authoring-byte-order little-endian;

server-duid "\000\001\000\001'\217@\304\010\000'\307\306 ";

lease 175.16.1.100 {
  starts 1 2021/01/11 17:00:56;
  ends 1 2021/01/11 17:05:56;
  cltt 1 2021/01/11 17:00:56;
  binding state active;
  next binding state free;
  rewind binding state free;
  hardware ethernet 08:00:27:ca:dd:fc;
  client-hostname "cserver";
}
lease 175.16.1.101 {
  starts 1 2021/01/11 17:00:57;
  ends 1 2021/01/11 17:05:57;
  cltt 1 2021/01/11 17:00:57;
  binding state active;
  next binding state free;
  rewind binding state free;
  hardware ethernet 08:00:27:80:3c:9d;
  client-hostname "cclient";
}
lease 175.16.1.100 {
  starts 1 2021/01/11 17:00:56;
  ends 1 2021/01/11 17:05:56;
  tstp 1 2021/01/11 17:05:56;
  cltt 1 2021/01/11 17:00:56;
  binding state free;
  hardware ethernet 08:00:27:ca:dd:fc;
}
lease 175.16.1.101 {
  starts 1 2021/01/11 17:00:57;
  ends 1 2021/01/11 17:05:57;
  tstp 1 2021/01/11 17:05:57;
  cltt 1 2021/01/11 17:00:57;
  binding state free;
  hardware ethernet 08:00:27:80:3c:9d;
}

SRC;
 }
?>

<html><head><meta charset="UTF-8"></head><body>
<script>
 function conv() {
   convert(document.getElementById('ta').value,document.getElementById('ta2'));
 }
 
 function k(e) {
   if ((e.ctrlKey || e.metaKey) && (e.keyCode == 13 || e.keyCode == 10)) {
     e.preventDefault();
     conv();
     var e=document.getElementById("title");e.innerHTML=e.innerHTML.replace(/ \([\s\S]*?\)/g, '');
  }
 }

 function tabulate(csv,p) {
  var ss=csv.split('\n');
  var s='<table style="text-align:center">';
  for(var i=0;i<ss.length;i++) {
   s += i==0?'<tr style="color:white;background-color:#ee4444;">':i%2==0?'<tr style="">':'<tr>';
   var ss2= ss[i].split(',');
   for(var j=0;j<ss2.length;j++) {
    s += (i==0?'<th>':'<td>')+ss2[j]+(i==0?'</th>':'</td>');
   }
   s += '</tr>';    
  }
  p.innerHTML = s +'</table>';
 }

 function convert(s,ta2) {
   var xhttp = new XMLHttpRequest();
   xhttp.open("POST", "<?php basename($_SERVER["SCRIPT_FILENAME"]);?>" , true);
   xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
   xhttp.onload = function() {ta2.innerHTML = xhttp.responseText;
    tabulate(xhttp.responseText,document.getElementById('p'));};
   xhttp.send("ta="+ encodeURIComponent(s));
 }

 function readFile(file) {
   var f = new XMLHttpRequest();
   f.open("GET", file, false);
   f.onreadystatechange = function () {
     if(f.readyState === 4) {
       if(f.status === 200 || f.status == 0) {
         document.getElementById('ta').innerHTML = f.responseText;
         document.getElementById('ta').focus();
       }
     }
   }
   f.send(null);
 }
 
 function readLocalFile(e) {
   var file = e.files[0];
   if (!file) {return;}
   var reader = new FileReader();
   reader.onload = function(e) {
     document.getElementById('ta').innerHTML = e.target.result;
     document.getElementById('ta').focus();
   };
   reader.readAsText(file);
 }

 function saveFile(ta,fname){
    var text = ta.value.replace(/\n/g, "\r\n");
    var blob = new Blob([text], { type: "text/plain"});
    var a = document.createElement("a");
    a.download = fname;
    a.href = window.URL.createObjectURL(blob);
    a.target ="_blank";
    a.style.display = "none";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
 }
</script>

<center>
 <h1 id="title">dhcp2kea</h1>
 <table style="width:960px">
  <tr>
   <td style="width:480px">
    <div style="color:white;background-color:#009900;width:480px;"> 
     &nbsp;<input type="file" onchange="readLocalFile(this);" />
     &nbsp;<button onclick="readFile('example1-dhcpd4.leases')">1</button>
     &nbsp;<button onclick="readFile('example2-dhcpd6.leases')">2</button>
     &nbsp;<button onclick="readFile('example3-dhcpd6.leases')">3</button>
    </div>
    <textarea autofocus id="ta" name="ta" onkeypress="k(event);" autofocus wrap="off"
     style="width:480px;height:480px;font-family:courier;font-size:14;"><?=$src?></textarea> 
   </td>
   <td style="width:480px">
    <div style="color:white;background-color:#0099cc;width:480px;">
     <span style="position:relative;left:-18px;"><button onclick="conv();">>></span></button>
     <button style="float:right" onclick="saveFile(document.getElementById('ta2'),'a.csv')">Save</button>
    </div>
    <textarea id="ta2" name="ta2" wrap="off" 
     style="width:480px;height:480px;font-family:courier;font-size:14">
    </textarea>
   </td>
  </tr>
 </table>
</center> 
 
<pre id="p"></pre>

</body></html>
