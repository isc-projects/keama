# Kea Migration Assistant

Kea migration assistant (keama) is an **experimental** tool that works as an
assistance tool that helps migration from ISC-DHCP (that now reached its
End-Of-Life) into Kea, which is a modern DHCP implementation. Keama is based on
ISC DHCP source code. It is able to read an isc-dhcp configuration and produce a
JSON configuration in Kea format. It should be used as a standalone
configuration conversion utility. We do not recommend running this code in a
production DHCP server. The `keama` tool converts ISC DHCP configuration.

To convert ISC DHCP lease file, please see `keama-leases` tool, described below.

## Limitations

This utility will translate most of your ISC DHCP configuration to the
appropriate format for Kea. Some elements of your ISC DHCP configuration can not
be automatically translated to Kea format. This is because some features of ISC
DHCP are either unsupported, or it work in a different way in Kea vs. ISC DHCP.
Where the utility is unable to translate the configuration, it will insert
messages highlighting what was not translated, with references to issues in the
Kea Gitlab that provide more detail. These sections of the configuration will
require manual review and adjustment.

This Kea migration assistant does not translate the current DHCP lease file.
This works on configuration files only. There is an experimental project to
translate lease files [here](https://gitlab.isc.org/isc-projects/keama-leases).

## Packaged version

ISC provides RPM/deb packages on Cloudsmith here: https://cloudsmith.io/~isc/repos/keama/packages/

## Building from source code

```bash
# First fetch the sources from git repository:
git clone https://gitlab.isc.org/isc-projects/keama.git

#  Configure the build.  If you want to install it somewhere specific use --prefix=<path> parameter
./configure

# Run make to build keama
make

# Install it (optional)
sudo make install
```

The `keama` binary will be available in the `keama` directory.

## Using the Kea migration assistant:

```bash
./keama  {-4|-6} -i <input ISC DHCP config file> -o <output file> -l <hook library path>
```

## Reporting issues

Please share your experiences with the migration. You can open bugs and feature
requests on the project page: https://gitlab.isc.org/isc-projects/keama/-/issues/new

## For developers

Tests are described [here](doc/system-tests.md).

## Web interface

See [doc/web.md](doc/web.md) file that explains how to set up and run basic web
interface for Keama.

# Keama Leases Migration

This tool facilitates one-way migration from ISC DHCP, which is now End of Life, to Kea, a newer DHCP server, also from ISC. Translating and transferring active leases is only one step in the process of migration from the obsolete system to the newer one. Because the lease files record the active state of the system, this step should be done when the old system is ready to be decommissioned.

Example files are provided for demonstration purposes only. The user should use their own current ISC DHCP lease file as the input file, and then load the output lease file into the target Kea server.

This tool is used to migrate the lease file, which contains the running state ("address assignments" or "leases") of your DHCP server. The other, even more important aspect is migration of your configuration file, which can be done using KeaMA (the Kea Migration Assistant).


## Running keama-leases

You need to get the keama-leases.py into your system. You can simply download it from this website or use git to clone the whole repository:

```
git clone https://gitlab.isc.org/isc-projects/keama-leases.git
```

You can run it with python interpreter. Any modern python 3 will do:

```
$ python leases/keama-leases.py

Usage: python3 leases.py ["[addr ]key=value"] [-e[dateTtime] [-v] [-h] [-d] [-t] leases-filename(s)
Options:
  -h : help - prints this help
  -v : verbose - print results to stdout
  -d : debug - print internal state
  "key=value" : default values for each lease
  "addr key=value" : default value for addr
  -e[dateTtime] : expire - overriding expire value with datetime (defaults to current)
  -t[n] : test - execute test/demo functions, optional n=1,2,3,4,6 (internal test no) defaults to all
Key names:
  uid, subnet_id, client-hostname, lease_type, prefix_len,
  hwaddr, state, user_context, hwtype, hwaddr_source,
  expire, valid_liftime
Examples:
  1.setting subnet_id=2 for all leases:
    python3 keama-leases.py "subnet_id=2" example1-dhcpd4.leases
  2.setting subnet_id=3 for 192.168.1.12 to 192.168.1.16 and subnet_id=2 for 192.168.1.20 to 192.168.1.29
    python3 keama-leases.py "192.168.1.1[2-6] subnet_id=3" "192.168.1.2\d subnet_id=2" example1-dhcpd4.leases
  3.setting subnet_id=3 for 192.168.1.12 and subnet_id=8 for 3ffe:501:ffff:100::c9d9 in two processed files
    python3 keama-leases.py "192.168.1.12 subnet_id=3" example1-dhcpd4.leases "3ffe:501:ffff:100::c9d9 subnet_id=8" example2-dhcpd6.leases
  4.setting expiration time to January,4th 2023 10 o'clock localtime for all leases
    python3 keama-leases.py -e2023/01/04T10:00:00 example1-dhcpd4.leases
  5.setting liftime of each lease starting from current time for internal test no 6 (dhcp6 string)
    python3 keama-leases.py "valid_liftime=3600" -e -t6
```

Typical usage would take a single parameter - the lease file you want to convert. Since there is no concept of `subnet-id` in the ISC DHCP and `subnet-ids` are required in Kea, there is an additional step. Typically, once your configuration is migrated using KeaMA, you should enable lease checks and set it to `fix`. This will tell Kea to attempt to find appropriate subnets from the configuration and correct subnet-ids for the leases. To do so, your Kea configuration should contain the following snippet:

```
"sanity-checks": {
        "lease-checks": "fix-del"
    },
```

For more details, see [Kea ARM Section 8.2.25](https://kea.readthedocs.io/en/kea-2.2.0/arm/dhcp4-srv.html#sanity-checks-in-dhcpv4) and [Kea ARM Section 9.2.25](https://kea.readthedocs.io/en/kea-2.2.0/arm/dhcp6-srv.html#sanity-checks-in-dhcpv6).

## Background

During its operation, the DHCP server temporarily assigns (leases) IPv4 addresses, IPv6 addresses, and IPv6 prefixes to end devices. The devices (clients) renew leases and eventually the leases are returned to the server when the client goes off-line. Each time a lease state changes, an entry is written to a lease file. A lease file is essentially a journal file. The same principle is used by both ISC DHCP server and Kea servers, however the syntax is different. The goal of this project is to come up with a software solution that would allow converting ISC DHCP lease file to Kea lease file syntax.

Since the lease file typically contains many entries for the same lease (e.g. an address is leased to client A, client A renews 3 times, client A goes away and the address expires, server then grants the address to client B) would result in many lease entries. From the conversion perspective, only the last entry is important. The earlier entries can be discarded.

There are two versions of the DHCP protocol. DHCP for IPv4 addresses is referred to as DHCPv4, and DHCPv6 handles IPv6 addresses and prefixes. While the overall concept is the same, there are some differences. DHCPv4 uses the concept of MAC address and client-id whereas DHCPv6 uses the DUID to identify clients. DHCPv6 has a preferred lease lifetime in addition to the valid lifetime parameter in DHCPv4. DHCPv6 leases can be for an address (ia_na) or prefix (ia_pd). The prefix has an additional parameter prefix length. For addresses, the prefix length should be 128.

## Kea lease file format - the target

Kea uses CSV notation for lease files. The columns have the following meaning:

- address: IPv4 or IPv6 address
- hwaddr: hardware (MAC) address (this field is DHCPv4 only)
- client_id: client identifier (this field is DHCPv4 only)
- duid: DHCP Unique Identifier, similar to MAC address, but may be longer (this field is DHCPv6 only)
- valid_lifetime: expressed in seconds
- pref_lifetime: expressed in seconds (this field is DHCPv6 only)
- lease_type: specifies type of a lease - address or prefix (this field is DHCPv6 only)
- iaid: Identity Association Identifier, a value client sends in DHCPv6 (this field is DHCPv6 only)
- prefix_len: prefix length (1-128 for prefixes, always 128 for addresses) (this field is DHCPv6 only)
- expire: expiration time, expressed as UNIX time (seconds since midnight 1 Jan 1970 UTC)
- subnet_id: identifier of the subnet the lease belongs to
- fqdn_fwd - a boolean flag specifying if forward DNS update (A or AAAA record) was conducted (forward fully qualified domain name)
- fqdn_rev - a boolean flag specifying if a reverse DNS update (PTR record) was conducted (reverse fully qualified domain name)
- hostname - name of the host
- state - state of the lease, typically 0
- user_context - contains any additional information and for this migration may be left empty.
- hwtype - hardware type, can be left empty
- hwaddr_source - source of the hardware address, can be left empty

DHCPv4 lease file in Kea:
```
address,hwaddr,client_id,valid_lifetime,expire,subnet_id,fqdn_fwd,fqdn_rev,hostname,state,user_context
192.0.2.4,3a:3b:3c:3d:3e:3f,33:30,40,1642000000,50,1,1,example-workstation1,0,
192.0.2.5,,31:32:33,40,1643210000,50,1,1,,1,{  }
192.0.2.6,32:32,,40,1643212345,50,1,1,three&#x2cexample&#x2ccom,2,{ "a": 1&#x2c "b": "c" }
```

DHCPv6 lease file in Kea:
```
address,duid,valid_lifetime,expire,subnet_id,pref_lifetime,lease_type,iaid,prefix_len,fqdn_fwd,fqdn_rev,hostname,hwaddr,state,user_context,hwtype,hwaddr_source
2001:db8::10,32:30,30,1642000000,1,50,1,60,70,1,1,example-workstation1,38:30,0,,90,16
2001:db8:abcd::11,32:31,30,1643210000,2,50,1,60,70,1,1,,38:30,1,{  },90,1
2001:db8:5555::12,32:32,30,1643212345,3,50,1,60,70,1,1,three&#x2cexample&#x2ccom,38:30,2,{ "a": 1&#x2c "b": "c" },90,4
```

## Additional considerations

1. subnet_id - Both servers have defined subnets that the addresses are allocated from. Kea requires the use of subnet-ids, which are non-existent in ISC DHCP. For the sake of migration, two approaches could be used. One is to simply use "subnet_id" and run kea with sanity-checks set to repair the values, as described in the [Kea ARM](https://kea.readthedocs.io/en/kea-2.2.0/arm/dhcp4-srv.html#sanity-checks-in-dhcpv4). Another approach would be to use the Kea migration assistant (keama), see [keama article](https://kb.isc.org/docs/migrating-from-isc-dhcp-to-kea-dhcp-using-the-migration-assistant) or [keama sources](https://gitlab.isc.org/isc-projects/dhcp/-/tree/master/keama).

2. uid to client-id - The field is called `uid` in ISC DHCP and `client-id` in Kea. The formatting is different. ISC DHCP uses a colon-separated hexadecimal list or as a quoted string.   If it is recorded as a quoted string and it contains one  or  more  non-printable characters,  those characters are represented as octal escapes - a backslash character followed by three octal digits. Kea just uses hex notation separated by colons (similar to MAC addresses). The migration tool will handle this translation.

3. hostname - Since Kea uses commas to separate values, the actual comma needs to be encoded with `&#x2c`. The migration tool will handle this translation.

4. Consider the size of your lease. We are aware of lease files that are over 2GB, although many of those are just renewing the same leases many times. Loading the entire lease file to memory may not the best strategy.

5. Leases have a start time, valid lifetime and expiration time bound by a simple rule: start + lifetime = expiration. ISC DHCP records start and end (as absolute value in human readable form), while Kea uses valid-lifetime (in seconds) and expiration time in UNIX time (seconds since midnight 1 jan 1970 UTC). The migration tool will handle this translation.

6. The fqdn_fwd and fqdn_rev fields are not explicitly specified in ISC DHCP. They could be set to true if hostname is non-empty. It may be useful to add a knob to overwrite their values as needed. This is a piece of local network configuration that the admin doing conversion should know if the DNS updates were done or not.

7. Contrary to its name, the ISC DHCP lease file may contain other information, in addition to the leases. This information will not be migrated to the Kea lease file.

## Example lease files

Several files were added to `examples/` directory. These are:

- example1-dhcp4.conf - config file for ISC DHCP. This is not really needed,
  except perhaps it's useful to generate more data.
- example1-dhcp4.leases - this file contains lease information. This is the input
  file.
- example1-kea-leases4.csv - this is the converted (output) file.
- example1-kea4.conf - this is an example kea config file that allows starting
  Kea and loading the .csv file.

The conversion was done by hand and may contain some mistakes.

## Converter source code

 The `leases/` directory contains:

- [keama-leases.py](leases/keama-leases.py) - converter source code written in Python3
- [keama-leases.php](leases/keama-leases.html) - the driver for running the converter from a web-browser with simple user interface
- [keama-leases.html](leases/keama-leases.html) - web interface for testing the converter with syntax colored items
- [keama-leases.png](leases/keama-leases.png) - web interface screendump

## Useful documentation

- On-line version of the `dhcp.leases` man page: https://kb.isc.org/docs/isc-dhcp-41-manual-pages-dhcpdleases. The actual man page is available in [dhcp source code](https://gitlab.isc.org/isc-projects/dhcp/-/blob/master/server/dhcpd.leases.5/)
