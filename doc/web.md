# Web interface

There is a simple web interface for keama written in Flask/python.


# Setting up

You probably want to set a local virtual environment. It's not necessary, but
it's very convenient as all the changes will be contained to this local env.
And you won't need root access to install dependencies in the `/usr/local` of
your system.

```shell
$ python -m venv venv
$ source venv/bin/activate
```

Now you need to install all python requirements:

```shell
$ pip install -r web/requirements.txt
```

# Running web interface (developers)

You can run Flask application locally, using built-in small web server that
comes with internal debugger. It is strongly discouraged to run it that way
in production.

```shell
$ python -m flask --app web.app:app run
```

You may want to run `python -m flask run --help` to get extra options, such
as `--host`, `--port` or even `--cert` and `--key` if you want to test it
locally over TLS.

Alternatively, you can run the `web/app.py` script:

```shell
$ python web/app.py
```

The uploaded files are stored in location governed by UPLOAD_FOLDER. See
`app.py` configuration. It's configured by default to be `web/data/` directory.

You need to place the keama binary somewhere in the system. If it's in a location
other than default `keama/keama`, you should tweak `KEAMA_PATH` in `app.py`. For information
how to compile Keama, see [README](../README.md).

# SECRET_KEY

Keama web interface comes with randomly generated `SECRET_KEY` stored persistently in `web/secret_key.py` file.
If you prefer to have your own `SECRET_KEY`, it is possible to do so by setting `FLASK_SECRET_KEY` environment variable.
Flask will load the `SECRET_KEY` from environment variable when booting. Another option to alter your `SECRET_KEY` is
to modify it inside of `web/secret_key.py` file, once it has been generated. In any case, please remember to generate
random `SECRET_KEY` according to official flask documentation: https://flask.palletsprojects.com/en/latest/config/#SECRET_KEY.

# Running web interface (production)

The built-in Flask debug web server is not appropriate for production use. For
that, a proper web server should be used. This example explains how to set up
`gunicorn` for that.

```shell
$ python -m venv venv
$ source .env/bin/activate
$ pip install -r web/requirements.txt
$ pip install gunicorn
$ python -m gunicorn -w 4 'web.app:app'
```

You might experience `ModuleNotFoundError: No module named 'app'` error when booting `gunicorn`.
In this case you need to set `PYTHONPATH` environment variable to point to your `web/` location,
and then try to start the server again.

E.g.
```shell
$ export PYTHONPATH=/path/to/keama/web/:${PYTHONPATH}
```

You need to place the keama binary somewhere in the system. If it's in a location
other than default `keama/keama`, you should tweak `KEAMA_PATH` in `app.py`. For information
how to compile Keama, see [README](../README.md).

# Running unit tests

There is a set of tests for web interface in `tests/test_web.py`. You can run it the usual way:

```shell
$ pip install pytest
$ python -m pytest -s -v --ignore=bind/
```

Make sure you have the virtual environment set up and dependencies installed.

# Used external libraries

Keama web interface is using external JavaScript libraries released under the terms of the MIT license:
* jQuery (https://jquery.com/) - used for simplifying some JS code, DOM dynamic changes, AJAX, dynamic styling, Bootstrap dependency
* Moment.js (https://momentjs.com/) - used for handling date and time
* clipboard.js (https://clipboardjs.com/) - used for handling `copy to clipboard` buttons
* modernizr (https://modernizr.com/) - used for determining browser support of html, css and JS features
* Bootstrap (https://getbootstrap.com/) - used for layout and site look and feel
* popper.js (https://popper.js.org/) - used for handling `copy to clipboard` buttons, Bootstrap dependency

Some of the libraries are accessed via CDNs. For security reasons, SRI checking is used. 
> The `integrity` and `crossorigin` attributes are used for [Subresource Integrity (SRI) checking](https://www.w3.org/TR/SRI/).
> This allows browsers to ensure that resources hosted on third-party servers have not been tampered with.
> Use of SRI is recommended as a best-practice, whenever libraries are loaded from a third-party source.
> Read more at [srihash.org](https://www.srihash.org).
