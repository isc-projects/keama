# Continuous Integration

The jobs are defined in `.gitlab-ci.yml`. There are three stages defined:

- checks (for relatively quick tasks such as linters and checkers)
- build (that builds the actual keama binary)
- tests (that run python tests, this step requires the binary)

In the future, we will want to add manual jobs here to deploy:
- to internal system
- to public dhcp.isc.org instance.

## Pylint

The ultimate goal is to reach maximum (10.0) score in pylint. For the time being,
we're not there yet. Please ensure that your changes do not lower the score. If
needed, tweak the `.pylint` config or add exceptions to the source file, *if*
you have a good reason for it.
