# Keama system tests

Keama system tests were originally written in bash, and are now converted to
pytest. Usage of a standard python framework has a number of advantages:

- easier test control: In particular running one, group or all tests
  (`pytest` will run everything, `pytest -k positive` will run all tests that have
  'positive' in its name, which can also be used to run one specific test);
- better reporting: by default, each run ends with a list of failed tests,
  number of passed, deselected (not run) and total tests, and execution time;
- more flexibility: `pytest --help` shows lots and lots of extra tuning tweaks,
  which custom-made framework has no chance to match;
- easier automation: it is easy to run `pytest` in a way that will generate
  output xml with the results, so QA team could integrate this with our CI
  processes.
- colored output, caching results, and more

# Installation & Running

### 1. Virtual environment
It is easy to set up the environment for the tests. The
first step is to set up a virtual environment. This step is not strictly
necessary. It lets you install python dependencies locally, without affecting
your whole OS. This has an advantage if you run multiple projects, don't want or
can't run commands as root, or don't want to "pollute" your whole system with
random dependencies.

```shell
python -m venv venv
source venv/bin/activate
```

### 2. Python dependencies 
Install the dependencies with commands:

```shell
pip install -r web/requirements.txt
pip install -r tests/requirements.txt
```

### 3. Correct `keama` path
If you haven't done that already, compile the `keama` binary
([building from source code](https://gitlab.isc.org/isc-projects/keama/-/tree/master#building-from-source-code)). 
By default, the `keama` binary is considered to be under `keama/keama` path. If for any reason you have customized
the path, you should modify the `KEAMA_PATH` variable in `tests/test_web.py`, in order to make the tests work.

### 4. Running the tests
Run pytest with command:
```shell
python -m pytest -s -v --ignore=bind/
```

> ### Note
> These commands were tested on Ubuntu 22.04 and macOS 12.6, but should work on other systems.

# Running system tests

Pytest has many useful options that can be employed. See `pytest --help` for
complete list. Here's several of the more useful ones:

- by default pytest suppresses stdout/stderr and attempts to just print the test
  status. If you're debugging tests, you almost always want to have this
  disabled. Use `-s` to disable.

- you may want to enable the pytest framework to be more verbose, in particular
  with logging more details why test passed. Use `-v` for that.

- if you want to run specific test or group of tests, use `-k expression`. This
  will run all the tests that contain expression in their name.

- there are some pytest tests in BIND9. You might want to add `--ignore=bind/`
  to ignore those.

Tests are also run automatically in GitLab CI. For details, see [GitLab CI](ci.md).

# Test Development

## Test layout

Tests are written in python, in `tests/test_*.py` files. This is pretty standard
layout. While there are other tests, the great majority of tests fall into
either positive (see `test_conversions_positive` or
`test_conversions_negative`). Both test types consist of mostly input config
files for ISC-DHCP and extra files. Positive tests have Kea config file that the
Keama is supposed to generate. Those are stored in `.out` file. The negative
tests have `.msg` file that contain expected error message that Keama is
supposed to print.

Some information is stored in the filenames. There is a concept of trail, i.e. a
suffix that contains extra information about how to run specific test. For
example, `.in4` means that Keama is supposed to run with `-4`.

Extensions:
 - .err4 = source for error test in DHCPv4
 - .errF = source for error test in DHCPv4 with -r fatal
 - .errP = source for error test in DHCPv4 with -r pass
 - .err6 = source for error test in DHCPv6
 - .err  = source for error test in DHCPv4 and DHCPv6
 - .msg  = result (first line of standard error) for error test
 - .in4  = source for working test in DHCPv4
 - .in6  = source for working test in DHCPv6
 - .ind  = source for working test in DHCPv4 with -D
 - .inD  = source for working test in DHCPv6 with -D
 - .inn  = source for working test in DHCPv4 with -N
 - .inN  = source for working test in DHCPv6 with -N
 - .inl  = source for working test in DHCPv4 with -l $HOOK
 - .inL  = source for working test in DHCPv6 with -l $HOOK
 - .outl = result for working test with default hook library path
 - .outL = result for working test with default hook library path
 - .out  = result for working test

The body of the name of a working test must include 4 or 6 so
the .out can be submitted to kea-dhcp4 or kea-dhcp6

There is a long list of cases in `test_keama.py` in `POSITIVE_CASES` and
`NEGATIVE_CASES` arrays. This can be useful to disable tests that are not yet
ready or for whatever reason should be disabled for a long time.

The files themselves are stored in `keama/tests/data` directory.

## Legacy tests

The original shell tests are still available in `keama/tests/` directory
in the form of `runone.sh`, `runall.sh`, `checkone.sh` and `checkall.sh`.
The `run*.sh` scripts have all been reimplemented in python (see
`test_keama.py`). The `check*.sh` script were not converted. To see a basic
documentation for the original scripts, see `keama/tests/README` file.

## Adding new tests

If the new system test follows the standard input vs expected output, you should
add extra files to `keama/tests/data` directory and then extend either
`POSITIVE_CASES` or `NEGATIVE_CASES` in `test_keama.py`. This is probably the
easiest approach to writing new tests.

You can also write proper python based test. A good examples are
`test_binary_present` and `test_help_file`.

There's a couple basic utility functions that might be useful (`run_cmd` which
runs an arbitrary external command and captures its exit code, stdout and
stderr; `check_output` that searches for a list of strings in output and couple
others).
