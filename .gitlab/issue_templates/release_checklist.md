---
name: a.b.c release checklist
about: Create a new issue using this checklist for each release.
---

# Keama Release Checklist


1. Gitlabab pipeline results:
    1. [ ] Check Gitlab [build](https://gitlab.isc.org/isc-projects/keama/-/pipelines) jobs for failures
    1. [ ] Check Gitlab [pytest](https://gitlab.isc.org/isc-projects/keama/-/pipelines) jobs for failures

1. Tarball preparation:
    1. [ ] If this is the release of the final version, please check the sanity check ticket of the previous release and make sure all comments are addressed
    1. [ ] bump up version in configure.ac
    1. [ ] bump up version in `tests/test_keama.py:test_version()` (isc-projects/keama#40)
    1. [ ] Note release in changelog
    1. [ ] check the date in LICENSE
    1. [ ] Check README file (including installation details)
    1. [ ] update copyrights in all touched files using a simple script in [qa-dhcp](https://gitlab.isc.org/isc-private/qa-dhcp/-/blob/master/kea/build/update-copyright-dates.py).
    1. [ ] Do `autoreconf -i` on a modern system, test the code compiles and push your changes.
1. Build tarball, packages and docker image
    1. [ ] Go to [keama-tarball](https://jenkins.aws.isc.org/view/isc-dhcp-dev/job/dhcp-dev/job/keama-tarball/) > Build with Parameters, in field `keamaBranch` put in release branch and run the job, this will build release tarball and save it as an artifact of the job
    1. [ ] Go to [keama-pkg](https://jenkins.aws.isc.org/view/isc-dhcp-dev/job/dhcp-dev/job/keama-pkg/) > Build with Parameters, in field `keamaBranch` put in release branch and run the job, this will build release packages and save it as an artifact of the job (`baseBranch` should be 'master' if not using custom qa-dhcp branch)
    1. [ ] Go to [keama-docker](https://jenkins.aws.isc.org/view/isc-dhcp-dev/job/dhcp-dev/job/keama-docker/) > Build with Parameters, in field `keamaBranch` put in release branch and run the job, this will build release tarball and save it as an artifact of the job (`keamadockerBranch` should be 'master' if not using custom keama-docker branch)
    1. [ ] Go to [keama-release-notes](https://jenkins.aws.isc.org/view/isc-dhcp-dev/job/dhcp-dev/job/keama-release-notes/) > Build with Parameters, in field `version` put in release version and run the job, this will build release notes in txt format and save it as an artifact of the job.
    1. [ ] before tarball will be deemed as ready to release it will be `release candidate`. Each consecutive respin will have it's own name starting from `-rc1`
    1. [ ] open a ticket in keama repo called `release X.Y.Z-rcX sanity checks` and put there location of release tarball, packages, and docker image
    1. [ ] wait for team input about new tarball, if respin is needed go back to `Build tarball` point also increasing release candidate number
    1. [ ] if tarball is accepted create a tag of this version on a last commit in release branch and kea-docker repo
    1. [ ] mark jenkins jobs to save forever.
    1. [ ] move tarball and release notes to non release candidate location (e.g. moving  to /data/shared/sweng/keama/releases/4.5.0)
    1. [ ] make sure that new release directory allow group write e.g. `chmod 665 /data/shared/sweng/keama/releases/4.3.2b1`
    1. [ ] upload tarball to cloudsmith
    1. [ ] upload packages to cloudsmith
    1. [ ] upload docker image to cloudsmith
    1. [ ] open tickets to address issues mentioned in sanity checks IF those were not already fixed and close sanity check ticket
1. Signing and notification
    1. [ ] Sign the tarball and put signature files with the tarball in release directory at repo.isc.org
        - You need to have your ISC Release Engineer GPG imported.
        - example command: `gpg --local-user "USER_FINGERPRINT" --armor --digest-algo SHA512 --detach-sign "FILENAME"`.
        - See [Kea script](https://gitlab.isc.org/isc-private/qa-dhcp/-/blob/master/kea/build/sign_kea_and_upload_asc.sh) for inspiration.
    1. [ ] notify support about readiness of release, at this point QA and dev team work is almost done.
1. Post release
    1. [ ] Bump version in `configure.ac`, `tests/test_keama.py:test_version()` (isc-projects/keama#40) and do `autoreconf -i`
    1. [ ] bump up keama version inside [keama-docker](https://gitlab.isc.org/isc-projects/keama-docker) [Dockerfile](https://gitlab.isc.org/isc-projects/keama-docker/-/blob/master/Dockerfile)
    1. [ ] push to repo and unfreeze the code
1. Releasing tarball
    - [ ] ***(Support)*** Wait for clearance from Security Officer to proceed with the public release (if applicable).
    - [ ] ***(Support)*** Confirm that the tarballs are signed correctly.
    - [ ] ***(Support)*** Place tarballs in public location on FTP site.
    - [ ] ***(Marketing)*** Write release email to *dhcp-announce*.
    - [ ] ***(Marketing)*** Write email to *dhcp-users* (if a major release).
    - [ ] ***(Marketing)*** Announce on social media.
    - [ ] ***(Marketing)*** Write blog article (if a major release).