* 26 [func] marmo, tomek

    Imported keama-leases tool into the keama repository. The tool was
    originally developed by Marek Moszynski. Tomek did some minor tweaks
    to import the code.

    (Gitlab #53)

* 25 [bug] piotrek

    Updated default path to keama binary in KeaMA web app.
    It was changed to be inline with the path when building with default
    configure && make commands.
    (Gitlab #44)

* 24 [bug] piotrek

    Fixed path to ChangeLog.md file, which is different when KeaMA web service 
    is deployed from keama-docker image.
    (Gitlab #41)

Keama 4.5.0 released on September 20, 2023

* 23 [build] tomek

    The left-over server code is now removed. Documentation, python tests
    are now included in the release tarball.
    (Gitlab #35)

* 22 [func] piotrek

    Changelog is now displayed in Keama web interface, as a part of
    About KeaMA page.
    (Gitlab #30)

* 21 [func] piotrek

    CSRF protection was force enabled in Keama web interface.
    (Gitlab #34)

* 20 [func] piotrek

    Keama web flask random SECRET_KEY is now generated only once and stored
    in a file to make it persistent. This is to prevent issues with CSRF
    session token.
    (Gitlab #33)

* 19 [build] tomek

    All python tests fixed. Added pylint. Added CI pipeline that runs pylint,
    builds keama code, and runs tests.
    (Gitlab #23)

* 18 [func] piotrek

    Enhancements of keama web interface: whole web page was reformatted and is
    using now the same styling as official isc.org web page. The html form is
    sent asynchronously (ajax) and whole feedback to the user is displayed in
    asynchronous manner. Previous web interface was kept as a fallback legacy
    version in case JavaScript is not supported by browser (url `/legacy`).
    (Gitlab #31)

* 17 [func] piotrek

    Enhancements of keama web interface: when needed, feedback is presented
    to the user in form of closable alert section on top of the page.
    (Gitlab #29)

* 16 [func] piotrek

    Enhancements of keama web interface: when migration is done, display only
    those contents to the user which are available; feedback such as "file too
    large" or "your config has been shared" are now using the same base template
    for consistent styling; absolute file paths are hidden in stdout and stderr
    in production; added white-space wrapping for html pre tags - used to display
    stdout, stderr and kea generated config.
    (Gitlab #26)

* 15 [build] tomek

    Compilation fix for FreeBSD. Tested on FreeBSD 13.0, but other OS versions
    are likely affected, too.
    (Gitlab #6)

* 14 [func] tomek

    It is now possible to either upload a file or copy-paste its contents.
    It is now possible to add your comments if you share your configuration
    with ISC. The "share with ISC" button is now working properly.
    (Gitlab #25)

* 13 [doc] manu

    Add the Kea Migration Assistant Docker option to the "Off-Line Migration"
    section.

* 12 [build] tomek

    Fixed compilation error on Alpine 3.17. Also disabled -Werror option
    that might cause compilation failures in other cases.
    (Gitlab #24)

* 11 [doc]  vicky

    Web interface reworded.
    (Gitlab #17)

* 10 [bug] fdupont

    Fixed potential infinite loop in handling pick-first-value and possibly
    other statements.
    (Gitlab #7)

* 9 [func] tomek

    The keama now can report its version (using `-v` or `--version`. It now
    prints some information about the file being converted and its content.
    The internal help is now corrected. Thanks to Carsten Strotmann for
    reporting and sending a patch.
    (Gitlab #14)

* 8 [func] tomek

    The web interface now has configurable timeout for keama conversion.
    This is useful to kill hanging processes if keama gets stuck. The default
    timeout is 30 seconds.
    (Gitlab #21)

* 7 [func] tomek

    Basic web interface implemented for keama. The interface is written in
    python and uses Flask framework.
    (Gitlab #13)

* 6 [build] tomek

    Converted system tests to python (pytest).
    (Gitlab #2)

* 5 [build] tomek

    Removed many obsolete files, added new, short README and ChangeLog files.
    (Gitlab #1)

* 4 [build] fdupont

   Fixed build by gcc10.
   (Gitlab dhcp#116)

* 3 [doc] fdupont

   New documentation including this file.
   (Gitlab dhcp#34)

* 2 [bug] fdupont

   Fixed dhcp4 option 67 wrong name.
   (Gitlab dhcp#22)

* 1 [func] fdupont

   Initial revision.

For complete code revision history, see
    http://gitlab.isc.org/isc-projects/keama

LEGEND
* [bug]   General bug fix.  This is generally a backward compatible change,
          unless it's deemed to be impossible or very hard to keep
          compatibility to fix the bug.
* [build] Compilation and installation infrastructure change.
* [doc]   Update to documentation. This shouldn't change run time behavior.
* [func]  New feature.  In some cases this may be a backward incompatible
          change, which would require a bump of major version.
* [sec]   Security hole fix. This is no different than a general bug
          fix except that it will be handled as confidential and will cause
          security patch releases.

*: Backward incompatible or operational change.
