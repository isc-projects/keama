import os
import tempfile
import unittest

from web.app import app, gen_changelog


class BasicTests(unittest.TestCase):
    def setUp(self):
        # We might tweak those for testing purposes eventually.
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False  # by default, disable CSRF protection in tests
        self.app = app.test_client()

    def tearDown(self):
        pass

    def setUpWithCsrfEnabled(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = True
        self.app = app.test_client()

    def test_main_page(self):
        """
        Check if the main welcome page loads and has a way to upload the config file.
        """
        self.setUpWithCsrfEnabled()
        response = self.app.get('/', follow_redirects=True)

        # Let's do some basic checks.
        self.assertEqual(response.status_code, 200)
        # Make sure there's a form in the response, that it has a field to choose a file and submit button.
        # There will be other fields and the description will probably change in the future, so let's not
        # be too picky about the wording or extra fields.
        self.assertTrue(b"Upload your configuration file" in response.data)
        self.assertTrue(b'<form id="migrate-form" class="migrate" enctype="multipart/form-data">' in response.data)
        self.assertTrue(b'<input id="csrf_token"' in response.data)
        self.assertTrue(b'<button id="fileSelect"' in response.data)
        self.assertTrue(b'<input type="radio" name="family"' in response.data)
        self.assertTrue(b'<input type="radio" name="share"' in response.data)
        self.assertTrue(b'<button type="submit"' in response.data)
        self.assertTrue(b'<input type="file" id="fileElem" name="file"' in response.data)  # config via file upload
        self.assertTrue(b'<textarea id="configtxt" name="configtxt"' in response.data)  # config via textarea
        self.assertTrue(b'<textarea name="comment"' in response.data)  # shared comment

    def test_keama_migration_page_get_forbidden(self):
        """The only method for accessing /migrate is POST (user uploads a file)"""
        response = self.app.get('/migrate', follow_redirects=True)
        self.assertTrue(b'Method Not Allowed' in response.data)
        self.assertEqual(405, response.status_code)

    def test_keama_migration_page_no_file(self):
        """This time we use proper method (POST), but there's no file uploaded. Check the error message."""
        response = self.app.post('/migrate', follow_redirects=True)
        self.assertEqual(400, response.status_code)
        self.assertTrue(b'You did not provide ISC DHCP config for migration!' in response.data)

    def test_keama_migration_page_no_csrf_token(self):
        """This time we use proper method (POST), but there's no CSRF token. Check the error message."""
        self.setUpWithCsrfEnabled()
        response = self.app.post('/migrate', follow_redirects=True)
        self.assertEqual(400, response.status_code)
        self.assertTrue(b'CSRF token is missing' in response.data)

    # TODO: Write tests that actually do the migration. That's gonna require some extra effort.
    # In particular, we'll need to set up the keama binary and have some valid (and invalid) configs.
    # It's doable, but that's out of scope for now.

    def test_download_page_missing(self):
        """Check if attempt to download non-existent file is handled properly."""
        # We didn't specify which file to download (and there aren't any), so it should give us 404
        response = self.app.get('/download/foo.txt', follow_redirects=True)
        self.assertTrue(b'not found' in response.data)
        self.assertEqual(404, response.status_code)

    def test_download_page_ok(self):
        """Let's create some fake file and ensure it can be downloaded."""
        # First create the file.
        fd, path = tempfile.mkstemp(suffix=".json")
        folder, fn = os.path.split(path)
        self.app.application.config['UPLOAD_FOLDER'] = folder

        response = self.app.get(f"/download/{fn}", follow_redirects=True)
        self.assertEqual(200, response.status_code)

        # Delete temp file after the test.
        os.close(fd)
        os.unlink(path)

    def test_health_check(self):
        """The health-check is now very basic. It will be more robust in the future."""
        response = self.app.get('/flask-health-check', follow_redirects=True)
        self.assertEqual(200, response.status_code)
        self.assertTrue(b"success" in response.data)

    # Tests related to the legacy (JS-free) version of the app.
    def test_legacy_main_page(self):
        """
        Check if the main welcome page loads and has a way to upload the config file.
        """
        self.setUpWithCsrfEnabled()
        response = self.app.get('/legacy', follow_redirects=True)

        # Let's do some basic checks.
        self.assertEqual(200, response.status_code)
        # Make sure there's a form in the response, that it has a field to choose a file and submit button.
        # There will be other fields and the description will probably change in the future, so let's not
        # be too picky about the wording or extra fields.
        self.assertTrue(b"upload your ISC DHCP configuration file" in response.data)
        self.assertTrue(b'<form method="post" action="/keama" enctype="multipart/form-data">' in response.data)
        self.assertTrue(b'<input id="file" name="file" type="file">' in response.data)
        self.assertTrue(b'<input id="submit" name="submit" type="submit"' in response.data)
        self.assertTrue(b'csrf_token' in response.data)

    def test_legacy_keama_migration_page_get_forbidden(self):
        """The only method for accessing /keama is POST (user uploads a file)"""
        response = self.app.get('/keama', follow_redirects=True)
        self.assertEqual(405, response.status_code)
        self.assertTrue(b'Method Not Allowed' in response.data)

    def test_legacy_keama_migration_page_no_file(self):
        """This time we use proper method (POST), but there's no file uploaded. Check the error message."""
        response = self.app.post('/keama', follow_redirects=True)
        self.assertEqual(200, response.status_code)
        self.assertTrue(b'You did not provide ISC DHCP config for migration!' in response.data)

    def test_legacy_share_file_get_forbidden(self):
        """The only method for accessing /share is POST (user agrees to share a config file)"""
        response = self.app.get('/share', follow_redirects=True)
        self.assertEqual(405, response.status_code)
        self.assertTrue(b'Method Not Allowed' in response.data)

    def test_legacy_share_file_missing(self):
        """This time we use proper method (POST), but there's no file to be shared. Check the error message."""
        response = self.app.post('/share', follow_redirects=True)
        self.assertEqual(404, response.status_code)
        self.assertTrue(b'not found' in response.data)

    def test_gen_changelog(self):
        """This test checks if html changelog is generated from ChangeLog.md, and it has correct content."""
        gen_changelog()
        changelog_abs_path = os.path.join(self.app.application.root_path, "templates", "changelog.html")
        self.assertTrue(os.path.exists(changelog_abs_path))
        expected_html = '''
<ul>
<li>1 [func] fdupont</li>
</ul>
<p>Initial revision.</p>
'''
        with open(changelog_abs_path, "r") as f:
            content = f.read()
            self.assertTrue(expected_html in content)


if __name__ == "__main__":
    unittest.main()
