import os
import glob
from subprocess import Popen, PIPE
from typing import List
import pytest

KEAMA_PATH = "keama/keama"
DATA_PATH = "tests/data"
HOOK_PATH = "/tmp/hooks-test-path"

POSITIVE_CASES = [
    "bintadx6",
    "bootfilename4",
    "charcasedx4",
    "class4empty",
    "class4",
    "class6empty",
    "class6",
    "concatdx4",
    "concatnulldx4",
    "configdata4",
    "dbtimeformat4",
    "dbtimeformat6",
    "ddnsupdstyle6",
    "defaultexpr6",
    "denyunknown6",
    "duiden6",
    "duidll6",
    "duidllhw6",
    "duidllt6",
    "duidlltthw6",
    "enableupdates6",
    "encodedx6",
    "escapestring4",
    "execstatement4",
    "execstatement6",
    "existsbx4",
    "filename4",
    "global4",
    "global6",
    "groupclass4",
    "groupclass6",
    "groupgroup4",
    "grouphost4",
    "groupsubnet4",
    "groupsubnet6",
    "hardware2dx4",
    "hardwaredx4",
    "hostidentifier4",
    "hostname4",
    "hostuid4",
    "ifxsc4",
    "ipaddr6",
    "ipaddrhost4",
    "lifetime4",
    "lifetime6",
    "lifetimedef4",
    "lifetimedef6",
    "minimal4",
    "minimal6",
    "noauth4",
    "noauth6",
    "notbx4",
    "notnotbx4",
    "nxdomainnx6",
    "onxsc4",
    "optdatagrouppool4",
    "optiondata4",
    "optiondata6",
    "optiondatapool4",
    "optiondatapool6",
    "optiondecl4",
    "optiondecl6",
    "optiondeclBat4",
    "optionencap4",
    "optionencap6",
    "optionexpr4",
    "optionspace4",
    "optionspace6",
    "optionvendor4",
    "optionvendor6",
    "orphan4",
    "orphan6",
    "permitauth4",
    "permitauth6",
    "permitknown4",
    "pickdx6",
    "pool42",
    "pool4",
    "pool6",
    "preferred6",
    "prefix62",
    "prefix6",
    "qualifyingsuffix4",
    "qualifyingsuffix6",
    "range4",
    "range6",
    "reversedx6",
    "shareone4",
    "shareone6",
    "sharepools4",
    "sharetwo4",
    "sharetwo6",
    "spawning6",
    "subclass4",
    "subclass6",
    "subclassbinsel4",
    "subclassbinsel6",
    "subclassguard4",
    "subclassguard6",
    "subnet4auth",
    "subnet4",
    "subnet6auth",
    "subnet6multi",
    "subnet6one",
    "subnet6",
    "substringdx4",
    "suffixdx4",
    "switchxsc4",
    "switchxsc6",
    "temporary6",
    "vendorspace4",
    "zone4",
]

NEGATIVE_CASES = [
    "badcasexsc",
    "badclass2",
    "badclass",
    "baddecl2array",
    "baddecl2record",
    "baddeclBt",
    "baddefaultxsc",
    "badduid",
    "badinclude",
    "badoption66",
    "badoptiond4",
    "badoptionI4",
    "badstatusdir",
    "badsubclass",
    "classbadmatch",
    "classbadmatchif",
    "classinclass",
    "duid2",
    "duidennoid",
    "duidennonum",
    "duidllbadtype",
    "duidllnohw",
    "duidlltbadtype",
    "duidlltnohw",
    "duidlltnotime",
    "duidlltthw4",
    "duidnoid",
    "fixedaddressinroot4",
    "fixedaddressinroot6",
    "fixedprefixinroot",
    "fqdncompressed",
    "groupinclass",
    "groupsubnetif",
    "hardwareinroot",
    "hostinclass",
    "hostinhost",
    "hostnum",
    "listarray",
    "mixedarray",
    "nestarray",
    "noclass",
    "noinclude",
    "nosubclass",
    "nosuperclass",
    "pool6in4",
    "poolinroot4",
    "poolinroot6",
    "prefix0",
    "prefix128",
    "prefixinroot6",
    "range6in4",
    "rangeinroot4",
    "rangeinroot6",
    "share0",
    "share2if",
    "shareempty",
    "shareinclass",
    "shareinhost",
    "shareinshare",
    "shareinsubnet4",
    "shareinsubnet6",
    "sharenoname",
    "subnet42if",
    "subnet4badmask",
    "subnet4inclass",
    "subnet4inhost",
    "subnet4nomask",
    "subnet62if",
    "subnet6inclass",
    "subnet6inhost",
    "subnet6nolen",
    "subnet6noslash",
    "subnetinsubnet4",
    "subnetinsubnet6",
    "tautology",
    "tautologyhexa",
    "tautologysub",
    "textarray",
    "unknownoption",
    "unknownspace",
    "userclass",
    "vendorclass",
]

def run_cmd(binfile, params, env=None):
    """Runs file specified as binfile, passes all parameters.
    Returns a tuple of returncode, stdout, stderr"""
    par = [binfile]
    if params and len(params):
        par += params

    # @todo: set passed env variable, if any
    p = Popen(par, stdout=PIPE, stderr=PIPE)
    stdout, stderr = p.communicate()
    return p.returncode, stdout.decode("utf-8"), stderr.decode("utf-8")

def check_output(output: str, strings: List[str]):
    """Checks if specified output (presumably stdout) has appropriate content. strings
    is a list of strings that are expected to be present. They're expected
    to appear in the specified order, but there may be other things
    printed in between them. Will assert if string is not found. Returns number
    of found strings."""
    offset = 0
    cnt = 0
    for s in strings:
        new_offset = output.find(s, offset)

        if new_offset == -1:
            assert False, f"Not found an expected string: '{s}' in '{output}'"

        offset = new_offset + len(s)
        cnt += 1
    return cnt

def check_file(file, strings):
    """Checks if specified file contains specified strings."""
    with open(file, "r") as f:
        content = f.read()

    return check_output(content, strings)

def check_command(bin, params, exp_code=0, exp_stdout=[], exp_stderr=[]):
    """Runs specified command (bin) with parameters (params) and expected
    the return code to be exp_code. Returns the (exit code, stdout, stderr) tuple."""

    # Run the command
    result = run_cmd(bin, params)
    print(f"\nRunning command: {bin} {' '.join(params)}, exit code: {result[0]}, " +
            f"stdout size: {len(result[1])}, stderr size: {len(result[2])}")

    # TODO: Interpret exp_code 0 as non-failure (i.e. different than 255)
    if exp_code == 0:
        assert result[0] != 255
    else:
        assert result[0] == exp_code

    if exp_stdout:
        print(f"Checking if stdout (size {len(result[1])}) equals expectations.")
        check_output(result[1], exp_stdout)
    if exp_stderr:
        print(f"Checking if stderr (size {len(result[2])}) equals expectations.")
        check_output(result[2], exp_stderr)

    return result

def trail_to_options(trail: str) -> list[str]:
    """Returns a list of options for keama based on the last character
    of the filename extension."""

    tto = {
        'r': "-4", # This is really the last r in .err.
        '4': "-4",
        '6': "-6",
        "F": "-4 -r fatal",
        "P": "-4 -r pass",
        "d": "-4 -D",
        "D": "-6 -D",
        "n": "-4 -N",
        "N": "-6 -N",
        "l": f"-4 -l {HOOK_PATH}/",
        "L": f"-6 -l {HOOK_PATH}/"
    }
    if trail in tto.keys():
        return tto[trail].split()

    assert False, f"Invalid trail (file extension) {trail}"

def check_positive(fname_base: str, input_dir: str, out_dir: str) -> bool:
    """This mimics the run_one.sh script. It takes a filename base (without
    extension) and input and output directories. Depending on the input
    file's extension (fname_base.in*), it will run keama with different
    options. The conversion is expected to succeed. The output is then
    compared to the fname_base.out file."""

    # STEP 1: determine if the input file exists and the paths are valid.
    files = glob.glob(input_dir + os.sep + fname_base + ".in*")
    assert files, f"Missing file {input_dir}/{fname_base}.in* required for positive test."
    fname = files[0]

    # STEP 2: Make sure there's no .err file.
    assert len(glob.glob(input_dir + os.sep + fname_base + ".err*")) == 0

    # STEP 3: Convert "trail" (last filename extension's char) to
    # keama options.
    trail = fname[-1:]
    opts = trail_to_options(trail)

    # STEP 4: Prepare keama options, and expected output.
    exp_output_file = input_dir + os.sep + fname_base + ".out"
    output_file = str(out_dir) + os.sep + fname_base + ".out"
    params = opts + ['-i', fname, "-o", output_file ]

    # Read expected output
    with open(exp_output_file, "r") as f:
        exp_content = f.read()
    exp_code = 0

    exp_stdout = ["Reading", fname, "writing", output_file, "Parsed", "lines from the input", fname, "Wrote",
                  "bytes to Kea JSON output", output_file]

    # STEP 5: Run the command and check its exit code and stdout content
    check_command(KEAMA_PATH, params, exp_code, exp_stdout, [] )

    return check_file(output_file, [exp_content]) > 0

def check_negative(fname_base: str, input_dir: str, out_dir: str) -> bool:
    """This mimics the run_one.sh script. It takes a filename base (without
    extension) and input and output directories. Depending on the input
    file's extension (fname_base.err*), it will run keama with different
    options. The conversion is expected to fail. The error message printed
    is then compared to the fname_base.msg file."""

    # STEP 1: determine if the input file exists and the paths are valid.
    # The .err file contains config file that has some errors in it.
    files = glob.glob(input_dir + os.sep + fname_base + ".err*")
    assert files, f"Missing file {input_dir}/{fname_base}.err* required for negative test."
    fname = files[0]

    # STEP 2: Make sure there is a .msg file (expected error message).
    output_fnames = glob.glob(input_dir + os.sep + fname_base + ".msg")
    assert output_fnames, f"Missing error message file {input_dir}/{fname_base}.msg"
    output_fname = output_fnames[0]

    # STEP 3: Convert "trail" (last filename extension's char) to
    # keama options.
    trail = fname[-1:]
    opts = trail_to_options(trail)

    # STEP 4: Prepare keama options, expected output.
    params = opts + ['-i', fname]

    exp_code = 255
    with open(output_fname, "r") as f:
        exp_stderr = f.read()

    # STEP 5: Run the command and check its exit code, stderr content
    check_command(KEAMA_PATH, params, exp_code, [], [exp_stderr] )

    # No asserts called so far, it looks OK
    return True

# --- tests start here --------------------------

def test_binary_present():
    """Checks if the keama binary is there and is executable."""
    assert os.access(KEAMA_PATH, os.X_OK)

def test_help_file():
    """ Checks if keama has a help file and it prints parameters."""
    exp = ["Usage: keama",
            "[-4|-6] - specifies IP family. Mutually exclusive. One is mandatory.",
            "[-D] - define ISC-DHCP's minimum, default and maximum builtin lifetimes.",
            "[-N] - instead of using global, use subnet-specific reservations",
            "[-r {perform|fatal|pass} - what to do with hostnames",
            "[-l hook-library-path] - optionally specifies location of Kea Hooks dir",
            "[-i input-file] - specifies the input file (ISC DHCP config)",
            "[-o output-file] - specifies the output file (Kea config)",
            "[-v | --version] - print keama version."]
    check_command(KEAMA_PATH, [], 0, exp, [])

def test_version():
    """ Checks if Keama can report its version."""

    # TODO The version reported should be set automatically.
    exp = ["KEAMA 4.5.1-git"]
    check_command(KEAMA_PATH, ["-v"], 0, exp, [])
    check_command(KEAMA_PATH, ["--version"], 0, exp, [])


@pytest.mark.parametrize("filename", POSITIVE_CASES)
def test_conversions_positive(filename, tmp_path):

    assert check_positive(filename, input_dir = DATA_PATH, out_dir = tmp_path)


@pytest.mark.parametrize("filename", NEGATIVE_CASES)
def test_conversions_negative(filename, tmp_path):

    assert check_negative(filename, input_dir = DATA_PATH, out_dir = tmp_path)
